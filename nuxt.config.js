import * as meta from './content/site-meta.json'

export default {
  target: 'static',

  head: {
    title: 'Home',
    titleTemplate: `%s | ${meta.title}`,
    htmlAttrs: {
      lang: meta.lang,
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: meta.description },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  components: true,

  buildModules: ['@nuxt/typescript-build', '@nuxtjs/tailwindcss'],

  modules: ['@nuxtjs/pwa', '@nuxt/content'],

  pwa: {
    manifest: {
      lang: 'en',
    },
  },
}
